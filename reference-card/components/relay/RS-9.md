# RS-9 relay

Coil voltage: 9V

Contact rating:
AC 1A 125V
DC 2A 24V

## Layout

```
Ce     A1   B1   C1

Ce     A2   B2   C2
```

`Ce`: coil power input. Have no polarity.

When the coil is not powered, `An` and `Bn` is connected. (i.e. A1 and B2 is not
connected).

When the coil is powered up, `An` and `Cn` is connected.

