# UDEV rule

First, place the `99-platformio-udev.rules` to `/etc/udev/rules.d`.

Then, run

```
sudo udevadm control --reload-rules
sudo udevadm trigger
```

and possible

```
sudo usermod -a -G dialout $USER
sudo usermod -a -G plugdev $USER
```

You may need to

* Re-login if user groups are adjusted
* Re-plug the device

so that the change(s) will be effective.

