#include <Arduino.h>

// Laser module investigation script
//
// The laser module uses GND and VCC (no mark on module board) for power input.
// To turn on the laser, we should send a signal via S port.
//

void doCycle(int);

int LED_PIN = LED_BUILTIN;
int SIGNAL_PIN = 5;

void setup() {
    pinMode(LED_PIN, OUTPUT);
    pinMode(SIGNAL_PIN, OUTPUT);
    digitalWrite(LED_PIN, LOW);
    digitalWrite(SIGNAL_PIN, HIGH);
}

void loop() {
    for (int i = 100; i <= 1000; i += 100) {
        doCycle(i);
    }
    for (int i = 1000; i > 0; i -= 100) {
        doCycle(i);
    }
}

// It's strange that, we should set LED_PIN to HIGH in D1_mini
void doCycle(int delayMillis) {
    digitalWrite(LED_PIN, HIGH);
    digitalWrite(SIGNAL_PIN, HIGH);
    delay(delayMillis);
    digitalWrite(LED_PIN, LOW);
    digitalWrite(SIGNAL_PIN, LOW);
    delay(delayMillis);
}
