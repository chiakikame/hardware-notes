#include <Arduino.h>

const int TARGET_PIN = LED_BUILTIN;
const int INPUT_PIN = 3;

void setup() {
    pinMode(TARGET_PIN, OUTPUT);
    pinMode(INPUT_PIN, INPUT);
    digitalWrite(TARGET_PIN, LOW);
}

void loop() {
    bool led_on = false;
    bool previous_press_state = false;
    while (true) {
        int button_pressed = digitalRead(INPUT_PIN) == HIGH;

        if (previous_press_state != button_pressed) {
            led_on ^= 1;
        }

        previous_press_state = button_pressed;
    }
}

