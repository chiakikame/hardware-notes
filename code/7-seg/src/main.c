#include <Arduino.h>
#include <limits.h>

#define TOTAL_PINS (8)
#define TIME_FOR_EACH_DIGIT 1000
//             a, b, c, d, e, f, g, dp
int PINS[] = { 1, 2, 3, 4, 5, 6, 7, 8 };

/*
 *
 *  a
 * f b
 *  g
 * e c 
 *  d
 */
int PATTERNS[10][TOTAL_PINS] = {
    //a, b, c, d, e, f, g, dp
    { 1, 1, 1, 1, 1, 1, 0, 1}, // 0
    { 0, 1, 1, 0, 0, 0, 0, 0}, // 1
    { 1, 1, 0, 1, 1, 0, 1, 1}, // 2
    { 1, 1, 1, 1, 0, 0, 1, 0}, // 3
    { 0, 1, 1, 0, 0, 1, 1, 1}, // 4
    { 1, 0, 1, 1, 0, 1, 1, 0}, // 5
    { 1, 0, 1, 1, 1, 1, 1, 1}, // 6
    { 1, 1, 1, 0, 0, 0, 0, 0}, // 7
    { 1, 1, 1, 1, 1, 1, 1, 1}, // 8
    { 1, 1, 1, 1, 0, 1, 1, 0}, // 9
};

long prev_timestamp = LONG_MIN;


void setup() {
    for (int i = 0; i < TOTAL_PINS; i++) {
        pinMode(PINS[i], OUTPUT);
    }
}

void display_num(int num);

void loop() {
    long current_timestamp = millis();
    int current_num = 0;

    if (current_timestamp - prev_timestamp > TIME_FOR_EACH_DIGIT) {
        // display current number
        for (int i = 0; i < TOTAL_PINS; i++) {
            digitalWrite(PINS[i], PATTERNS[current_num][i] == 1 ? HIGH : LOW);
        }

        // mark display info
        prev_timestamp = current_timestamp;
        current_num += 1;
        if (current_num >= 10) {
            current_num = 0;
        }
    }
}

