#include <Arduino.h>

const int TARGET_PIN = LED_BUILTIN;

void setup() {
    pinMode(TARGET_PIN, OUTPUT);
}

void loop() {
    digitalWrite(TARGET_PIN, HIGH);
    delay(1000);
    digitalWrite(TARGET_PIN, LOW);
    delay(250);
    digitalWrite(TARGET_PIN, HIGH);
    delay(250);
    digitalWrite(TARGET_PIN, LOW);
    delay(1000);
}

