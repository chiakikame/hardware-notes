# Hardware (embedded) research storage

If you found any error in these materials, or if there's any licensing problem,
please do not hesitate to contact me :)

## Private area

**NOTE: private area are not included in this repository** due to licensing
issues (which won't be solved forever).

### `reference`

Reference documents about boards and widgets.

### `tutorial`

Store external tutorial documents, code samples etc.

## Public area

### `reference-card`

Personal reference card.

### `notes`

Personal notes.

### `code`

Personal code & projects

### `udev-rule`

Udev rule needed for uploading firmware.

### `schematic`

Breadboard design drawn with Fritzing.

Now, it's storage location of inscape-drawn schematics.
(Currently not using professional softwares like geda or kicad)

