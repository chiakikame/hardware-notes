# License of this repository

This repository is a shared personal notes, however, if you found these useful, you may use them
according to the following licenses.

## Codes

Codes are licensed under GPLv3 or higher. Please find full text of GPLv3 at 
https://www.gnu.org/licenses/gpl-3.0.en.html.

## Notes and schematic

The electronic library svg file is grabbed from
[Wikipedia](https://commons.wikimedia.org/wiki/File:Electrical_symbols_library.svg),
which is released under Public domain.

Other schematics, reference cards and notes are released under 
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

For those who uses the non-code materials in their work, please point the source of those materials
to this repository, so that other people can find other (possibly) useful resources too.

