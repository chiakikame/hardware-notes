# Note

## About unpowered LED light

Sometimes when we touch a grounded LED, it will have very dim light emitted. The
reason is stated [here](https://electronics.stackexchange.com/questions/87233/why-does-an-led-light-up-when-i-touch-it).

In essence, the body may act as an antenna, receiving the electromagnetic wave emitted from electronic appliance
nearby. Since that EM wave is in alternate form (AC), once one of LED's pin is grounded and we touched another,
it will have dim light.

This theory matches the continuity of the emitted light. Statics will not make the LED emit light continuously, and
we will feel the zap when LED is touched.

## Power stability: micro USB from transformer

TL;DR it's not stable.

The loading circuit looks like `schematic/content/20180731-0231-loadingTest2`,
except that only 3 * 100 ohm resistors are used as loading.

Before turning on the load: 5.2 V, after turning on the load: 4.6 V.

