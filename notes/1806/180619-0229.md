# Note about breadboard

See board `20180619-1216-switch`.

Remember that, don't put one element's input and output into connecting group
of breadboard.

For example, if we put one of the input side to `40a`, the other into `40b`, then
since all 5 nodes between `40a` and `40e` are interconnected by a wire who has nearly
no resistance, electrical current will definitely go that route, skipping the
widget.
